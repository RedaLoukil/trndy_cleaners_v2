from rest_framework.authtoken.models import Token
from rest_framework import serializers
from users.models import User
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate


class TokenSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(source='key')
    
    class Meta:
        model = Token
        fields = ("auth_token",)



class UserSerializerCreate(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'phone_number','first_name', 'last_name', 'password' , 'usertype')

    def validate(self, data):
        """
        Administrator permissions needed
        """

        if not is_administrator(self.context['request'].user):
            raise serializers.ValidationError(constants.PERMISSION_ADMINISTRATOR_REQUIRED)
        return data
    
    @staticmethod
    def validate_password(password):
        """
        Validate password
        """

        validate_password(password)
        return password

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User 
        fields = ('username','phone_number','email','first_name','last_name','password')

class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User 
        fields = ('username','phone_number','email','first_name','last_name','password')

class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    default_error_messages = {
        'inactive_account': 'User account is disabled.',
        'invalid_credentials': 'Unable to login with provided credentials.',
    }

    def __init__(self, *args, **kwargs):
        super(UserLoginSerializer, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self, attrs):
        self.user = authenticate(username=attrs.get("username"), password=attrs.get('password'))
        if self.user:
            if not self.user.is_active:
                raise serializers.ValidationError(self.error_messages['inactive_account'])
            return attrs
        else:
            raise serializers.ValidationError(self.error_messages['invalid_credentials'])