from django.db import models
from trndy_cleaners.accounts.models import Client


class Order(models.Model):
    client = models.OneToOneField(Client , on_delete=models.CASCADE)
    
    def __str__(self):
        return self.client
    


