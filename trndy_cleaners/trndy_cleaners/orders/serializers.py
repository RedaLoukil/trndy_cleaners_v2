from rest_framework import serializers
from trndy_cleaners.jobs.models import Order


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

class OrderCreateSerializer():
    class Meta:
        model = Order
        fields = '__all__'
    