from django.db import models
from trndy_cleaners.jobs.models import Order
from trndy_cleaners.accounts import
# Create your models here.

class Notification(models.Model):
    order = models.OneToOneField(Order , on_delete=models.CASCADE)
    
    def __str__(self):
        return self.order