import React from 'react';
import { ActivityIndicator, View , StyleSheet , Text , Image} from 'react-native';

class LoadingScreen extends React.Component {
    render(){
        return(
            <View style={style.root}>
                <Image style={{height:130 , width:95 , marginBottom:20}} source={require('../images/logo.png')}/>
                <Text style={{fontSize:30 , }}>Trndy Cleaners</Text>
            </View>
        )
    }
}

const style = StyleSheet.create({
    root : {
        backgroundColor: '#f8f8f8',
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        
    },
    
    
})

export default LoadingScreen;

