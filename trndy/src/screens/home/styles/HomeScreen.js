import EStyleSheet from 'react-native-extended-stylesheet';


const styles = EStyleSheet.create({
  root: {
    backgroundColor: '#333',
    justifyContent: 'center',
    flex: 1,
  }
});

export default styles;