import React , {Component} from 'react';
import LoadingScreen from '../../commons/LoadingScreen'
import {View , Text , StyleSheet , Button} from 'react-native'


class HomeScreen extends Component {
    state = {
        loading : true ,
    }
    componentDidMount(){
        setTimeout(()=> {
            this.setState({loading : false})
        }, 3000)
    }
    render(){
        if(this.state.loading){
            return (
                <LoadingScreen/>
            )
        }
        return(
            <View style={style.root}>
                <Text style={{fontSize:30 , marginBottom:100 }}>Trndy Cleaners</Text>
                <Button     
                    title="CONTINUE"
                    color="#FE434C" 
                    onPress={()=> {
                        console.log("clicked")
                    }}
                />
            </View>
        )
    }
}

const style = StyleSheet.create({
    root : {
        backgroundColor: '#f8f8f8',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        
    }
})

export default HomeScreen;