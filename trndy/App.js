import React from 'react';
import { StyleSheet, Text, View , StatusBar } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import {HomeScreen} from './src/screens/home'


export default class App extends React.Component {
  render() {
    return (
      
      <HomeScreen/>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
